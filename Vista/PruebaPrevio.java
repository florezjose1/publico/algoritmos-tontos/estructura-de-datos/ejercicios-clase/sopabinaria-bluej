package Vista;

import Interface.IBinario;
import Modelo.Bit;
import Negocio.SopaBinaria;

/**
 * Ejecución por consola de la sopa binaria
 * @author Jose Florez
 */
public class PruebaPrevio {

    public static void main(String[] args) {
        try {
            Bit matrizOriginal[][];
            Bit matrizOrdenada[][];

            // Leo la sopa binaria
            SopaBinaria miExcel = new SopaBinaria("Datos/binarios.xls", 0);
            // Cargo la matriz original
            matrizOriginal = miExcel.getMatriz();
            
            System.out.println("Matriz Original");
            System.out.println("************************");
            System.out.println(miExcel.toString());
            // obtengo la paridad por a parte para mostrarla por cada fila
            int paridad[] = miExcel.buscarParidad(matrizOriginal);
            System.out.println("\nParidad: Cada valor es una fila");
            for(int n : paridad) {
                System.out.print(n + "\t");
            }
            System.out.println("\n\n************************");


            // Cargo la matriz ordenada por pares
            // llamo el método a traves de la interfaz
            IBinario i_binario = miExcel;
            matrizOrdenada = i_binario.getMatrizOrdenada();
            // obtengo la paridad por a parte para mostrarla por cada fila
            int paridad1[] = miExcel.buscarParidad(matrizOrdenada);
            
            System.out.println("Matriz Ordenada");
            System.out.println("************************");
            System.out.println(miExcel.toString());
            // obtengo la paridad por a parte para mostrarla por cada fila
            System.out.println("\nParidad: Cada valor es una fila");
            for(int n : paridad) {
                System.out.print(n + "\t");
            }
            System.out.println("\n************************");
            
            // Mandamos a imprimir en el pdf la matriz y su paridad
            miExcel.imprimirParidadPDF(matrizOrdenada, paridad1, "Matriz Ordenada");
            System.out.println("PDF creado en: assets/docs/pdf_paridad_generado.pdf");
            
            // Probamos contructor error de valores
            miExcel = new SopaBinaria("Datos/binarios.xls", 1);
            

        } catch (Exception e) {
            System.err.println(e.getMessage());
            // e.printStackTrace();
        }
    }
}

package Vista;

import java.io.FileInputStream;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.fxml.FXML;
import Controlador.SopaBinariaControlador;

/**
 * Clase vista que se conecta con el fxml y este a su vez con el controlador para manejar los eventos.
 * 
 * @author Jose Florez
 * @version 1.0.0
 */

public class SopaBinariaVista extends Application{
    

    public void start(Stage stage) throws IOException{
        SopaBinariaControlador controllerInstance = new SopaBinariaControlador();
        // Create the FXMLLoader
        FXMLLoader fxmlLoader= new FXMLLoader();
        
        Pane root = (Pane) fxmlLoader.load(getClass().getResource("SopaBinaria.fxml"));        

        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        stage.setScene(scene);
        // Set the Title to the Stage
        stage.setTitle("Sopa Binaria");
        stage.resizableProperty().setValue(Boolean.FALSE);
        // Display the Stage
        stage.show();
    }

}

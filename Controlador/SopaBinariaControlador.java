package Controlador;

import java.io.File;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import Negocio.SopaBinaria; 

/**
 * Clase controlador quien maneja cada uno de los eventos lanzados por el usuario.
 * 
 * @author Jose Florez
 * @version 1.0.0
 */

public class SopaBinariaControlador {

    @FXML // fx:id="btnBuscarNumero"
    private Button btnBuscarNumero; // Value injected by FXMLLoader

    @FXML // fx:id="btnGenerarPDF"
    private Button btnGenerarPDF; // Value injected by FXMLLoader

    @FXML // fx:id="btnCargarDatos"
    private Button btnCargarDatos; // Value injected by FXMLLoader

    @FXML // fx:id="lbMensaje"
    private Label lbMensaje; // Value injected by FXMLLoader

    @FXML // fx:id="txtNumeroEntero"
    private TextField txtNumeroEntero; // Value injected by FXMLLoader

    SopaBinaria miExcel;

    public SopaBinariaControlador() throws IOException {
    }

    @FXML
    void cargarDatos() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("Excel xls", "*.xls"));
        File f = fc.showOpenDialog(null);
        
        if(f!= null) {
            try {
                miExcel = new SopaBinaria(f.getAbsolutePath(),0);
                lbMensaje.setText("¡Excel cargado exitosamente! Ahora ingresa un número y dale buscar");
                btnBuscarNumero.setDisable(false);
                txtNumeroEntero.setDisable(false);
            }catch (Exception e) {
                lbMensaje.setText(e.getMessage());
            }
        } else {
            lbMensaje.setText("No se ha seleccionado ningún archivo");
        }
        
    }
    
    private int getNumero() {
        return Integer.parseInt(txtNumeroEntero.getText());
    }

    @FXML
    void generarPDF() {
        try {
            miExcel.crearPDF(this.getNumero());
            lbMensaje.setText("PDF generado en assets/docs/pdf_generado.pdf");
        } catch (Exception e) {
            lbMensaje.setText(e.getMessage());
        }
    }

    @FXML
    void buscarNumero() {
        String mensaje = miExcel.buscarEnSopa(this.getNumero());
        lbMensaje.setText(mensaje);
        btnGenerarPDF.setDisable(false);
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert btnCargarDatos != null : "fx:id=\"btnCargarDatos\" was not injected: check your FXML file 'SopaBinaria.fxml'.";
        assert btnGenerarPDF != null : "fx:id=\"btnGenerarPDF\" was not injected: check your FXML file 'SopaBinaria.fxml'.";
        assert btnBuscarNumero != null : "fx:id=\"btnBuscarNumero\" was not injected: check your FXML file 'SopaBinaria.fxml'.";
        assert txtNumeroEntero != null : "fx:id=\"txtNumeroEntero\" was not injected: check your FXML file 'SopaBinaria.fxml'.";
        assert lbMensaje != null : "fx:id=\"lbMensaje\" was not injected: check your FXML file 'SopaBinaria.fxml'.";
        
        lbMensaje.setText("Hola, para empezar:  \n*Primero debes cargar un excel. \n\tLos formato de celdas permitido Texto, numero o general. " +
                          "\n*Luego ingresa un número y haz clic en 'Buscar número' para ver los resultados.");

    }
}

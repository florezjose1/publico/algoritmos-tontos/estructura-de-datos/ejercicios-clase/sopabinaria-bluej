package Test.Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;

import Modelo.Bit;
import Negocio.SopaBinaria;

/**
 * The test class SopaBinariaTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaBinariaTest
{
    private Bit matrizOriginal[][];
    private Bit matrizOrdenada[][];
    
    /**
     * Default constructor for test class SopaBinariaTest
     */
    public SopaBinariaTest()
    {
    }
    
    public void escenario() throws Exception {
        // Leo la sopa binaria
        SopaBinaria miExcel=new SopaBinaria("Datos/binarios.xls",0);
        // Cargo la matriz original
        this.matrizOriginal = miExcel.getMatriz();
        // Imprimo Matriz
        System.out.println("Matriz Original");
        for (Bit filas[] : this.matrizOriginal) {
             System.out.println(Arrays.toString(filas));
        }
        
        // Cargo la matriz ordenada por pares
        this.matrizOrdenada = miExcel.getMatrizOrdenada();
        System.out.println("Matriz Ordenada");
        for (Bit filas[] : this.matrizOrdenada) {
             System.out.println(Arrays.toString(filas));
        }
    }
    
    @Test
    public void testGetMatrizOrdenada() throws Exception {
        System.out.println("getMatrizOrdenada");
        // 1. Crear el escenario
        this.escenario();
        // 2. Valores esperados;
        Bit v1 = new Bit(true);
        Bit v2 = new Bit(true);
        Bit v3 = new Bit(false);
        
        // 3. Resultados
        Bit[][] result = this.matrizOrdenada;
        Bit r1 = result[0][0];
        Bit r2 = result[0][1];
        Bit r3 = result[0][2];
        
        //4. Ejecutar la prueba:
        assertEquals(v1.getValor(), r1.getValor());
        assertEquals(v2.getValor(), r2.getValor());
        assertEquals(v3.getValor(), r3.getValor());
        
        // Para la última fila se espera todo false
        Bit vf = new Bit(false);
        Bit rf1 = result[9][0];
        Bit rf2 = result[9][1];
        Bit rf3 = result[9][2];
        Bit rf4 = result[9][3];
        Bit rf5 = result[9][4];
        assertEquals(vf.getValor(), rf1.getValor());
        assertEquals(vf.getValor(), rf2.getValor());
        assertEquals(vf.getValor(), rf3.getValor());
        assertEquals(vf.getValor(), rf4.getValor());
        assertEquals(vf.getValor(), rf5.getValor());
        
    }
}

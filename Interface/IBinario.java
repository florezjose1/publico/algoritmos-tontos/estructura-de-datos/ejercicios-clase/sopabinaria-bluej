package Interface;


import Modelo.Bit;

/**
 * Creamos el métodos para probar la paridad
 * @author Jose Florez
 */
public interface IBinario {
    /**
     * Método de búsqueda de paridad en la matriz
     * @return Una nueva matriz ordenada de acuerdo a su paridad
     */
    public Bit[][] getMatrizOrdenada();
}
